import 'package:test/test.dart';

import 'package:searches_and_sorts/src/custom_exceptions.dart';
import 'package:searches_and_sorts/src/utils.dart';

List<int> emptyList = <int>[];
List<int> oneElement = <int>[9];
List<double> unsortedListDemo = <double>[103, 103, 9, -9.5, 11, 100, 102, 10];
List<double> sortedListDemo = <double>[-9.5, 9, 10, 11, 100, 102, 103, 103];
List<double> reverslySortedListDemo = <double>[
  103,
  103,
  9,
  -9.5,
  -10,
  -100,
  -102,
  -109.9
];

class MyType {
  MyType(this.parameter);
  String parameter;
}

void main() {
  setUp(() {
    emptyList = <int>[];
    oneElement = <int>[9];
    unsortedListDemo = <double>[103, 103, 9, -9.5, 11, 100, 102, 10];
    sortedListDemo = <double>[-9.5, 9, 10, 11, 100, 102, 103, 103];
    reverslySortedListDemo = <double>[
      103,
      103,
      9,
      -9.5,
      -10,
      -100,
      -102,
      -109.9
    ];
  });

  group('fallbackCompare', () {
    test('strings', () {
      expect(fallbackCompare('hey', 'after'), equals(1));
      expect(fallbackCompare('terminal', 'terminal'), equals(0));
      expect(fallbackCompare('bad', 'good'), equals(-1));
    });
    test('double-precision floating points', () {
      expect(fallbackCompare(9.9, 3.2), equals(1));
      expect(fallbackCompare(9.9, 4.4 + 5.5), equals(0));
      expect(fallbackCompare(1, 9.9), equals(-1));
    });
    test('integers', () {
      expect(fallbackCompare(9, 4), equals(1));
      expect(fallbackCompare(8, 8), equals(0));
      expect(fallbackCompare(-10, -1), equals(-1));
    });
    test('self defined type without compareTo', () {
      MyType a = MyType("hello");
      MyType b = MyType("hello");
      MyType c = MyType("ciao");
      //expect(fallbackCompare(a, b));
    });
  });
  group('fallbackCheckEquality', () {
    test('strings', () {
      expect(fallbackCheckEquality('hey', 'after'), equals(false));
      expect(fallbackCheckEquality('terminal', 'terminal'), equals(true));
      expect(fallbackCheckEquality('bad', 'good'), equals(false));
    });
    test('double-precision floating points', () {
      expect(fallbackCheckEquality(9.9, 3.2), equals(false));
      expect(fallbackCheckEquality(9.9, 4.4 + 5.5), equals(true));
      expect(fallbackCheckEquality(1, 9.9), equals(false));
    });
    test('integers', () {
      expect(fallbackCheckEquality(9, 4), equals(false));
      expect(fallbackCheckEquality(8, 8), equals(true));
      expect(fallbackCheckEquality(-10, -1), equals(false));
    });
  });
  group('swap', () {
    test('swap testing', () {
      // sortedListDemo = <double>[-9.5, 9, 10, 11, 100, 102, 103, 103]
      swap(sortedListDemo, 0, 1);
      expect(sortedListDemo,
          equals(<double>[9, -9.5, 10, 11, 100, 102, 103, 103]));
      swap(sortedListDemo, 1, 0);
      expect(sortedListDemo,
          equals(<double>[-9.5, 9, 10, 11, 100, 102, 103, 103]));
      swap(sortedListDemo, 2, 4);
      expect(sortedListDemo,
          equals(<double>[-9.5, 9, 100, 11, 10, 102, 103, 103]));
    });
  });
  group('isSorted function', () {
    test('empty list', () {
      expect(isSorted(emptyList), equals(true));
    });
    test('sorted list', () {
      expect(isSorted(sortedListDemo), equals(true));
    });
    test('unsorted list', () {
      expect(isSorted(unsortedListDemo), equals(false));
    });
    test('reversly sorted list', () {
      expect(isSorted(reverslySortedListDemo), equals(false));
    });
    test('sorted sublist of unsorted list', () {
      expect(isSorted(unsortedListDemo, left: 3, right: 7), equals(true));
    });
    test('unsorted sublist of unsorted list', () {
      expect(isSorted(unsortedListDemo, left: 3, right: 8), equals(false));
    });
    test('reversly sorted sublist of unsorted list', () {
      expect(isSorted(unsortedListDemo, left: 0, right: 4), equals(false));
    });
  });

  group('isReverslySorted function', () {
    test('empty list', () {
      expect(isReverslySorted(emptyList), equals(true));
    });
    test('sorted list', () {
      expect(isReverslySorted(sortedListDemo), equals(false));
    });
    test('unsorted list', () {
      expect(isReverslySorted(unsortedListDemo), equals(false));
    });
    test('reversly sorted list', () {
      expect(isReverslySorted(reverslySortedListDemo), equals(true));
    });
    test('sorted sublist of unsorted list', () {
      expect(
          isReverslySorted(unsortedListDemo, left: 3, right: 7), equals(false));
    });
    test('unsorted sublist of unsorted list', () {
      expect(
          isReverslySorted(unsortedListDemo, left: 3, right: 8), equals(false));
    });
    test('reversly sorted sublist of unsorted list', () {
      expect(
          isReverslySorted(unsortedListDemo, left: 0, right: 4), equals(true));
    });
  });

  group('findExtrema', () {
    test('empty list', () {
      expect(
          () => findExtrema(emptyList),
          throwsA(predicate((e) =>
              e is InvalidIndexException &&
              e.message == 'Invalid index for this operation.')));
    });
    test('one-element list', () {
      expect(
          findExtrema(oneElement), equals(<String, int>{'min': 9, 'max': 9}));
    });
    test('sorted list', () {
      expect(findExtrema(sortedListDemo),
          equals(<String, double>{'min': -9.5, 'max': 103}));
    });
  });
}
