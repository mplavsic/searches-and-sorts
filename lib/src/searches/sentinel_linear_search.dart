import '../utils.dart';

/// Returns position of [value] inside [list] using sentinel linear search.
///
/// If [left] (inclusive) and [right] (exclusive) are supplied, only that range
/// is considered.
///
/// If [areEqual] is omitted, this defaults to calling == between objects,
/// i.e., `object1 == object2`.
///
/// This function uses a sentinel, which avoids checking if the index is out of
/// bounds during every loop iteration. It is thought for huge lists.
///
/// Returns -1 if [value] is not inside [list].
int sentinelLinearSearch<E>(List<E> list, E value,
    {int left = 0, int? right, bool Function(E, E)? areEqual}) {
  right = RangeError.checkValidRange(left, right, list.length, 'left', 'right');

  if (list.isEmpty) {
    return -1; // The below instruction `E lastValue = list[right - 1];`
    // will throw a StateError otherwise.
  }

  areEqual ??= fallbackCheckEquality; // Assigned only if compare is null.

  E lastValue = list[right - 1]; // The last element's old value is saved...
  list[right - 1] = value; // ...and replaced with `value`, so that we can avoid
  // checking for `i < list.length` in the classic linear search algorithm,
  // since the loop will eventually have to terminate, i.e., at some point the
  // below `areEqual(list[i], value` will have to return true.

  int pos = left;
  while (!areEqual(list[pos], value)) {
    // We are skipping the simple linear search `i < list.length` comparison.
    pos++;
  }

  list[right - 1] = lastValue; // Restoring the old value of the last element.
  if ((pos < right - 1) || areEqual(list[right - 1], value)) {
    return pos;
  }

  return -1; // The value was not found.
}
