A well-documented and well-tested Dart library package containing some searching and sorting algorithms that work with generics.

## Usage

A simple usage example:

```dart
import 'package:searches_and_sorts/searches.dart';

main() {
  var list = <int>[-8, 4, 5, 8, 9, 20, 200, 882, 10000, 2000000];
  var posOfValue = binarySearch(list, 9); // 4
}
```

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: https://gitlab.com/mplavsic/searches-and-sorts/issues
