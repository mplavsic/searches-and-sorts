import 'package:test/test.dart';

import 'package:searches_and_sorts/exceptions.dart';
import 'package:searches_and_sorts/searches.dart';

late List<int> emptyList;
late List<String> oneString;
late List<int> oneElement;

void main() {
  late List<int> sortedListDemo;
  late List<int> unsortedListDemo;

  setUp(() {
    // Since sentinel linear search mutates the collection during its execution,
    // a set up before each test is essential.
    emptyList = <int>[];
    oneString = <String>['Demo'];
    oneElement = <int>[8];
    sortedListDemo = <int>[-37, 12, 13, 345, 463, 479, 802, 1000, 2111];
    unsortedListDemo = <int>[-37, -12, 13, 3453, 43, -47, -802, 1000, 2111, -9];
  });

  void testAllListElements(Function searchingAlgorithm, List testList) {
    for (int i = 0; i < testList.length; i++) {
      expect(searchingAlgorithm(testList, testList[i]), equals(i));
    }
  }

  void testFirstNaturalNumbers(Function searchingAlgorithm, List testList) {
    for (int i = 0; i < 200; i++) {
      if (!testList.contains(i)) {
        expect(searchingAlgorithm(testList, i), equals(-1));
      }
    }
  }

  void testEmptyList(Function searchingAlgorithm) {
    for (int i = 0; i < 200; i++) {
      expect(searchingAlgorithm(emptyList, i), equals(-1));
    }
  }

  void testOneElementList(Function searchingAlgorithm) {
    expect(searchingAlgorithm(oneString, 'Demo'), equals(0));
    expect(searchingAlgorithm(oneString, 'Other string'), equals(-1));
    expect(searchingAlgorithm(oneElement, 8), equals(0));
    expect(searchingAlgorithm(oneElement, 3), equals(-1));
  }

  void testPartialListSearch<E>(Function searchingAlgorithm, List<int> list,
      int left, int right, E value, int expectedIndex) {
    expect(searchingAlgorithm(list, value, left: left, right: right),
        equals(expectedIndex));
  }

  group('binary search: ', () {
    Function searchingAlgorithm = binarySearch;
    test('empty list', () {
      testEmptyList(searchingAlgorithm);
    });
    test('one element', () {
      testOneElementList(searchingAlgorithm);
    });
    test('all list elements (sorted list)', () {
      testAllListElements(searchingAlgorithm, sortedListDemo);
    });
    test('first natural numbers (sorted list)', () {
      testFirstNaturalNumbers(searchingAlgorithm, sortedListDemo);
    });
    test('unsorted list', () {
      expect(
          () => searchingAlgorithm(unsortedListDemo, 13),
          throwsA(predicate((e) =>
              e is UnsortedListException &&
              e.message == 'The given (sub)list is not sorted.')));
    });
    test('partial search with sorted list', () {
      testPartialListSearch(searchingAlgorithm, sortedListDemo, 0, 4, 463, -1);
      testPartialListSearch(searchingAlgorithm, sortedListDemo, 1, 6, 463, 4);
      testPartialListSearch(searchingAlgorithm, sortedListDemo, 1, 6, 100, -1);
    });
    test('partial search with sorted sublist of unsorted list', () {
      testPartialListSearch(searchingAlgorithm, unsortedListDemo, 1, 4, -12, 1);
      // -12 is inside `unsortedListDemo` and within the specified range.
      testPartialListSearch(searchingAlgorithm, unsortedListDemo, 1, 4, -9, -1);
      // -9 is inside `unsortedListDemo`, but not within the specified range.
    });
    test('partial search with unsorted sublist of unsorted list', () {
      expect(
          () => searchingAlgorithm(unsortedListDemo, 9, left: 1, right: 6),
          throwsA(predicate((e) =>
              e is UnsortedListException &&
              e.message == 'The given (sub)list is not sorted.')));
    });
  });

  group('linear search: ', () {
    Function searchingAlgorithm = linearSearch;
    test('empty list', () {
      testEmptyList(searchingAlgorithm);
    });
    test('one element', () {
      testOneElementList(searchingAlgorithm);
    });
    test('all list elements (sorted list)', () {
      testAllListElements(searchingAlgorithm, sortedListDemo);
    });
    test('all list elements (unsorted list)', () {
      testAllListElements(searchingAlgorithm, unsortedListDemo);
    });
    test('first natural numbers (sorted list)', () {
      testFirstNaturalNumbers(searchingAlgorithm, sortedListDemo);
    });
    test('first natural numbers (unsorted list)', () {
      testFirstNaturalNumbers(searchingAlgorithm, unsortedListDemo);
    });
    test('unsorted list', () {
      // This is relevant only for binary search, but let's test anyway.
      expect(searchingAlgorithm(unsortedListDemo, 13), equals(2));
    });
    test('partial search with sorted list', () {
      testPartialListSearch(searchingAlgorithm, sortedListDemo, 0, 4, 463, -1);
      testPartialListSearch(searchingAlgorithm, sortedListDemo, 1, 6, 463, 4);
      testPartialListSearch(searchingAlgorithm, sortedListDemo, 1, 6, 100, -1);
    });
    test('partial search with sorted sublist of unsorted list', () {
      testPartialListSearch(searchingAlgorithm, unsortedListDemo, 1, 4, -12, 1);
      // -12 is inside `unsortedListDemo` and within the specified range.
      testPartialListSearch(searchingAlgorithm, unsortedListDemo, 1, 4, -9, -1);
      // -9 is inside `unsortedListDemo`, but not within the specified range.
    });
    test('partial search with unsorted sublist of unsorted list', () {
      // This is relevant only for binary search, but let's test anyway.
      expect(searchingAlgorithm(unsortedListDemo, 9, left: 1, right: 6),
          equals(-1));
    });
  });

  group('sentinel linear search: ', () {
    Function searchingAlgorithm = sentinelLinearSearch;
    test('empty list', () {
      testEmptyList(searchingAlgorithm);
    });
    test('one element', () {
      testOneElementList(searchingAlgorithm);
    });
    test('all list elements (sorted list)', () {
      testAllListElements(searchingAlgorithm, sortedListDemo);
    });
    test('all list elements (unsorted list)', () {
      testAllListElements(searchingAlgorithm, unsortedListDemo);
    });
    test('first natural numbers (sorted list)', () {
      testFirstNaturalNumbers(searchingAlgorithm, sortedListDemo);
    });
    test('first natural numbers (unsorted list)', () {
      testFirstNaturalNumbers(searchingAlgorithm, unsortedListDemo);
    });
    test('unsorted list', () {
      // This is relevant only for binary search, but let's test anyway.
      expect(searchingAlgorithm(unsortedListDemo, 13), equals(2));
    });
    test('partial search with sorted list', () {
      testPartialListSearch(searchingAlgorithm, sortedListDemo, 0, 4, 463, -1);
      testPartialListSearch(searchingAlgorithm, sortedListDemo, 1, 6, 463, 4);
      testPartialListSearch(searchingAlgorithm, sortedListDemo, 1, 6, 100, -1);
    });
    test('partial search with sorted sublist of unsorted list', () {
      testPartialListSearch(searchingAlgorithm, unsortedListDemo, 1, 4, -12, 1);
      // -12 is inside `unsortedListDemo` and within the specified range.
      testPartialListSearch(searchingAlgorithm, unsortedListDemo, 1, 4, -9, -1);
      // -9 is inside `unsortedListDemo`, but not within the specified range.
    });
    test('partial search with unsorted sublist of unsorted list', () {
      // This is relevant only for binary search, but let's test anyway.
      expect(searchingAlgorithm(unsortedListDemo, 9, left: 1, right: 6),
          equals(-1));
    });
    test('list consistency before and after sentinel linear search', () {
      // Since sentinel linear search changes the last element and restores it
      // afterwards, it is necessary to test that the stored element is restored
      // correctly.
      List<int> clonedList = List<int>.from(unsortedListDemo);
      sentinelLinearSearch(clonedList, 4); // 4 is not inside `clonedList`.
      expect(clonedList, equals(unsortedListDemo));
      // at the end of the iteration both list must be the same.
      sentinelLinearSearch(clonedList, 802); // 802 is inside `clonedList`.
      expect(clonedList, equals(unsortedListDemo));
      // Again, the lists must match.
    });
  });
}
