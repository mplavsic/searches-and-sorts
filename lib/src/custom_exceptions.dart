/// Custom [Exception] that's thrown whenever the index is invalid.
class InvalidIndexException implements Exception {
  InvalidIndexException({this.message = 'Invalid index for this operation.'});
  final String message;
}

/// Custom [Exception] that's thrown whenever a sorted list is required, but an
/// unsorted one is provided.
class UnsortedListException implements Exception {
  UnsortedListException({this.message = 'The given (sub)list is not sorted.'});
  final String message;
}

/// Custom [Exception] that's thrown whenever [Comparable.compareTo]'s parameter
/// is not of the same type of the object calling it.
class InvalidTypeForCompareTo implements Exception {
  InvalidTypeForCompareTo({this.message = 'Invalid type of argument.'});
  final String message;
}
