import '../utils.dart';

/// Sorts a list using heapsort. You can opt to sort the list only
/// between [left] (inclusive) and [right] (exclusive).
///
/// If [compare] is omitted, this falls back to calling [Comparable.compareTo]
/// on the objects. In this case, the objects must be [Comparable].
///
/// This sorting algorithm is adequate for lists that contain many
/// elements, e.g., 40 or more.
///
/// Heapsort is not stable.
/// The sorting is performed in-place, therefore it doesn't requires extra
/// memory.
///
/// Worst-case time complexity: O(n^log(n))
/// Average-case time complexity: O(n^log(n))
/// Best-case time complexity: O(n^log(n))
/// Worst-case space complexity: O(log(1)) auxiliary memory
void heapsort<E>(List<E> list,
    {int left = 0, int? right, int Function(E, E)? compare}) {
  right = RangeError.checkValidRange(left, right, list.length, 'left', 'right');

  compare ??= fallbackCompare;
  // The next loop is Floyd's buildHeap algorithm implementation. It creates
  // a max-heap.
  int length = right - left;
  for (int i = left + (length / 2 - 1).floor(); i >= left; i--) {
    _siftDown(list, i, right, compare, left);
  }
  for (int i = left + length - 1; i >= left + 1; i--) {
    swap(list, i, left); // The first element of the (sub)list is moved to the
    // end of the list, since it is the max. Now, the elements within range
    // [`i`, `list.length-1`] (both extremes included) are sorted.
    _siftDown(list, left, i, compare, left);
    // The [left, left+1, ..., i-2, i-1] elements of `list` form a max-heap.
    // During the next loop iteration the max (index `left` in `list`) will
    // be moved to postion `i`, and this function will be called again to
    // fix the heap.
  }
}

/// Restores the max heap condition for the node at [list]'s position [pos] and
/// its children, grandchildren, etc.
/// The max heap ends at [endHeap] (exclusive).
///
/// This function works only if the children, granchildren, etc., of the
/// node at position [pos] are in a valid max-heap condition (therefore Flyod's
/// buildHeap algorithm needs to start from the nodes with only one "generation"
/// of children, and end with the root node).
///
/// The last parameter [left] is needed for computing the index of the children
/// nodes when working with a left boundary. If no boundary is specified when
/// calling [heapsort] (not the below [_siftDown]), [left] will be 0 (also for
/// [_siftDown]).
void _siftDown<E>(
    List<E> list, int pos, int endHeap, int Function(E, E) compare, int left) {
  int pos_child = (2 * (pos - left) + 1) + left; // why not `2 * pos + 1`?
  // Here `2 * pos + 1` would make sense only if the heapsort always considered
  // the whole list (therefore no sublists with left and/or right boundaries).
  if (pos_child + 1 < endHeap &&
      compare(list[pos_child], list[pos_child + 1]) < 0) {
    pos_child++; // We select the index of the bigger child as pos_child.
  }

  while (pos_child < endHeap && compare(list[pos], list[pos_child]) < 0) {
    swap(list, pos, pos_child);
    pos = pos_child;
    pos_child =
        (2 * (pos - left) + 1) + left; // Read above why not `2 * pos + 1`.

    if (pos_child + 1 < endHeap &&
        compare(list[pos_child], list[pos_child + 1]) < 0) {
      pos_child++;
    }
  }
}
