import '../utils.dart';

/// Returns position of [value] inside [list] using classic linear search.
///
/// If [left] (inclusive) and [right] (exclusive) are supplied, only that range
/// is considered.
///
/// If [areEqual] is omitted, this defaults to calling == between objects,
/// i.e., `object1 == object2`.
///
/// Ideal for unsorted lists. For sorted lists one should use binary search.
///
/// Returns -1 if [value] is not inside [list].
int linearSearch<E>(List<E> list, E value,
    {int left = 0, int? right, bool Function(E, E)? areEqual}) {
  right = RangeError.checkValidRange(left, right, list.length, 'left', 'right');

  areEqual ??= fallbackCheckEquality; // Assigned only if `areEqual` is null.

  for (int pos = left; pos < right; pos++) {
    if (areEqual(list[pos], value)) {
      return pos;
    }
  }
  return -1;
}
