import './custom_exceptions.dart';

/// Comparing function that asserts that its first argument is comparable.
int fallbackCompare(Object? object1, Object? object2) =>
    (object1 as Comparable<Object?>).compareTo(object2);

/// A [Comparable] comparator which can be used if the type is known to have
/// the [Comparable.compareTo] function already implemented,
int typeDefinedCompareTo<T extends Comparable<T>>(T a, T b) => a.compareTo(b);

/// Falls back to the `==` operator method for checking equality between two
/// objects.
bool fallbackCheckEquality(Object? object1, Object? object2) =>
    (object1 == object2);

/// Uses the `==` operator method for checking for equality. This should be
/// used if the type is known to have the `==` operator method defined.
bool typeDefinedEquals<T>(T a, T b) => (a == b);

void swap<E>(List<E> list, int index1, int index2) {
  RangeError.checkValidIndex(index1, list, 'index1');
  RangeError.checkValidIndex(index2, list, 'index2');
  E value_at_index_1 = list[index1];
  list[index1] = list[index2];
  list[index2] = value_at_index_1;
}

/// Checks if the list is sorted according to [compare].
bool isSorted<E>(List<E> list,
    {int left = 0, int? right, int Function(E, E)? compare}) {
  right = RangeError.checkValidRange(left, right, list.length, 'left', 'right');

  compare ??= fallbackCompare;

  int pos = left;
  int length = right - left;

  if (length > 1) {
    while (pos < right - 1) {
      if (compare(list[pos], list[pos + 1]) > 0) {
        return false;
      }
      pos++;
    }
  }

  return true;
}

/// Checks if the list is sorted in the opposite order according to [compare].
bool isReverslySorted<E>(List<E> list,
    {int left = 0, int? right, int Function(E, E)? compare}) {
  right = RangeError.checkValidRange(left, right, list.length, 'left', 'right');

  compare ??= fallbackCompare;

  int pos = left;
  int length = right - left;

  if (length > 1) {
    while (pos < right - 1) {
      if (compare(list[pos], list[pos + 1]) < 0) {
        return false;
      }
      pos++;
    }
  }

  return true;
}

/// Returns the min and max of a [List].
///
/// A [Map] containg keys `'min'` and `'max'` is returned.
Map<String, E> findExtrema<E>(List<E> list,
    {int left = 0, int? right, int Function(E, E)? compare}) {
  right = RangeError.checkValidRange(left, right, list.length, 'left', 'right');

  if (list.isEmpty) {
    throw InvalidIndexException();
  }

  compare ??= fallbackCompare;

  E min = list[left], max = list[left];

  for (int pos = left; pos < right; pos++) {
    if (compare(list[pos], min) < 0) {
      min = list[pos];
    } else if (compare(list[pos], max) > 0) {
      max = list[pos];
    }
  }

  return {'min': min, 'max': max};
}
