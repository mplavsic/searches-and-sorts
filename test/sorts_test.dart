import 'package:test/test.dart';

import 'package:searches_and_sorts/sorts.dart';

late List<int> emptyList;

late List<int> oneElement;

List<String> list1Sorted = <String>[
  'aba',
  'bbp',
  'ppp',
  't',
  'tu',
  'tz',
  'wra',
  'z',
  'z84'
];
late List<String> list1Variant1;
late List<String> list1Variant2;

List<int> list2Sorted = <int>[-37, 12, 13, 345, 463, 479, 802, 1000, 2111];
late List<int> list2Variant1;
late List<int> list2Variant2;

List<double> list3Sorted = <double>[-9.5, 9, 10, 11, 100, 102, 103, 103];
late List<double> list3Variant1;
late List<double> list3Variant2;

void main() {
  setUp(() {
    // Since sorting algorithms mutate the collections during their execution,
    // a set up before each test is essential.
    emptyList = <int>[];

    oneElement = <int>[8];

    list1Variant1 = <String>[
      'bbp',
      't',
      'aba',
      'tu',
      'z',
      'tz',
      'wra',
      'z84',
      'ppp'
    ];
    list1Variant2 = <String>[
      'z',
      'bbp',
      'aba',
      'tu',
      'ppp',
      'tz',
      'wra',
      't',
      'z84'
    ];

    list2Variant1 = <int>[12, 345, 13, 463, 802, 479, 1000, 2111, -37];
    list2Variant2 = <int>[1000, 12, 13, 463, 802, 345, 479, -37, 2111];

    list3Variant1 = <double>[-9.5, 9, 10, 11, 100, 102, 103, 103]; // sorted
    list3Variant2 = <double>[103, 103, 9, -9.5, 11, 100, 102, 10];
  });

  void runTests(Function sortingAlgorithm) {
    test('empty list', () {
      sortingAlgorithm(emptyList);
      expect(emptyList, equals(<int>[]));
    });
    test('list with only one element', () {
      sortingAlgorithm(oneElement);
      expect(oneElement, equals(<int>[8]));
    });
    test('list1Variant1', () {
      sortingAlgorithm(list1Variant1);
      expect(list1Variant1, equals(list1Sorted));
    });
    test('list1Variant2', () {
      sortingAlgorithm(list1Variant2);
      expect(list1Variant2, equals(list1Sorted));
    });
    test('list2Variant1', () {
      sortingAlgorithm(list2Variant1);
      expect(list2Variant1, equals(list2Sorted));
    });
    test('list2Variant2', () {
      sortingAlgorithm(list2Variant2);
      expect(list2Variant2, equals(list2Sorted));
    });
    test('list3Variant1', () {
      sortingAlgorithm(list3Variant1);
      expect(list3Variant1, equals(list3Sorted));
    });
    test('list3Variant2', () {
      sortingAlgorithm(list3Variant2);
      expect(list3Variant2, equals(list3Sorted));
    });
    test('sublist1Variant1', () {
      sortingAlgorithm(list1Variant1, left: 0, right: 7);
      // Subset to sort: ['bbp', 't', 'aba', 'tu', 'z', 'tz']
      expect(
          list1Variant1,
          equals(<String>[
            'aba',
            'bbp',
            't',
            'tu',
            'tz',
            'wra',
            'z',
            'z84',
            'ppp'
          ]));
    });
    test('sublist1Variant2', () {
      sortingAlgorithm(list1Variant2, left: 3, right: 7);
      // Subset to sort: ['tu','ppp','tz'].
      expect(
          list1Variant2,
          equals(<String>[
            'z',
            'bbp',
            'aba',
            'ppp',
            'tu',
            'tz',
            'wra',
            't',
            'z84'
          ]));
    });
    test('sublist2Variant1', () {
      sortingAlgorithm(list2Variant1, left: 4, right: list2Variant1.length - 1);
      // Subset to sort: [802, 479, 1000, 2111]
      expect(list2Variant1,
          equals(<int>[12, 345, 13, 463, 479, 802, 1000, 2111, -37]));
    });
    test('sublist2Variant2', () {
      sortingAlgorithm(list2Variant2, left: 2, right: list2Variant2.length - 1);
      // Subset to sort: [13, 463, 802, 345, 479, -37].
      expect(list2Variant2,
          equals(<int>[1000, 12, -37, 13, 345, 463, 479, 802, 2111]));
    });
    test('sublist3Variant1', () {
      sortingAlgorithm(list3Variant1, left: 1, right: list3Variant1.length);
      // Subset to sort: [9, 10, 11, 100, 102, 103, 103]
      expect(list3Variant1, equals(list3Sorted));
    });
    test('sublist3Variant2', () {
      sortingAlgorithm(list3Variant2, left: 1, right: list3Variant2.length - 2);
      // Subset to sort: [103, 9, -9.5, 11, 100]
      expect(
          list3Variant2, equals(<double>[103, -9.5, 9, 11, 100, 103, 102, 10]));
    });
  }

  group('heapsort: ', () {
    runTests(heapsort);
  });

  group('insertion sort: ', () {
    runTests(insertionSort);
  });

  group('merge sort: ', () {
    runTests(mergeSort);
  });

  group('selection sort: ', () {
    runTests(selectionSort);
  });
}
