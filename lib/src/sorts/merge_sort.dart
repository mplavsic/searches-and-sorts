import 'dart:collection';

import '../utils.dart';

/// Sorts a list using merge sort. You can opt to sort the list only
/// between [left] (inclusive) and [right] (exclusive).
///
/// If [compare] is omitted, this falls back to calling [Comparable.compareTo]
/// on the objects. In this case, the objects must be [Comparable].
///
/// This sorting algorithm is adequate for lists that contain many
/// elements, e.g., 40 or more.
///
/// Merge sort is stable. The sorting is not in-place.
///
/// Worst-case time complexity: O(n^log(n))
/// Average-case time complexity: O(n^log(n))
/// Best-case time complexity: O(n^log(n))
/// Worst-case space complexity: O(n) auxiliary memory
void mergeSort<E>(List<E> list,
    {int left = 0, int? right, int Function(E, E)? compare}) {
  right = RangeError.checkValidRange(left, right, list.length, 'left', 'right');

  compare ??= fallbackCompare;

  ListQueue<E> sortedList = _separate(list, left, right, compare);

  int pos = left;
  sortedList.forEach((element) {
    list[pos++] = element;
  });
}

ListQueue<E> _separate<E>(
    List<E> list, int min, int max, int Function(E, E) compare) {
  if (max - min == 0) {
    return ListQueue<E>(0);
  } else if (max - min == 1) {
    return ListQueue<E>.from([list[min]]);
  }
  int minLeft = min;
  int minRight = ((max - min) >> 1) + min;
  int maxLeft = minRight;
  int maxRight = max;
  return _merge(_separate(list, minLeft, maxLeft, compare),
      _separate(list, minRight, maxRight, compare), compare);
}

/// Merges [list1] and [list2] and returns a merged list.
ListQueue<E> _merge<E>(
    ListQueue<E> list1, ListQueue<E> list2, int Function(E, E) compare) {
  ListQueue<E> merged = ListQueue(list1.length + list2.length);
  while (list1.isNotEmpty || list2.isNotEmpty) {
    if (list1.isEmpty) {
      // Only `list2` remains.
      while (list2.isNotEmpty) {
        merged.add(list2.removeFirst());
      }
      break; // The loop can only finish either here or below.
    } else if (list2.isEmpty) {
      // Only `list1` remains.
      while (list1.isNotEmpty) {
        merged.add(list1.removeFirst());
      }
      break; // The loop can only finish either here or above.
    } // If both `list1` and `list2` still have some elements, then the smaller
    // one gets added to `merged` at this step.
    else if (compare(list1.first, list2.first) <= 0) {
      // The above `<= 0` makes this algorithm stable. `< 0` would still work,
      // but the implementation would become instable.
      merged.add(list1.removeFirst());
      continue;
    } else {
      merged.add(list2.removeFirst());
    }
  }
  return merged;
}
