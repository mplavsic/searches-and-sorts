import 'package:searches_and_sorts/exceptions.dart';
import 'package:searches_and_sorts/utils.dart';

List<String> emptyListDemo = <String>[];
List<double> oneElementListDemo = <double>[9.9];
List<int> multipleElementListDemo = <int>[
  1000,
  12,
  13,
  463,
  802,
  345,
  479,
  -37,
  2111
];

List<int> list1 = [2, 5, 6];
List<int> list2 = [2, 2, 2];
List<int> list3 = [6, 5, 2];
List<int> list4 = [5, 2, 6];

class MyType {
  MyType(this.parameter);
  String parameter;
}

class ObjectExtendedMyType extends Comparable {
  ObjectExtendedMyType(this.parameter);
  String parameter;

  @override
  int compareTo(other) {
    // `other` is of type `Object?` (therefore can be `null`).
    if (other is MyTypeExtendedMyType) {
      return this.parameter.compareTo(other.parameter);
    }
    throw InvalidTypeForCompareTo();
  }

  bool operator ==(Object? other) {
    if (other is ObjectExtendedMyType) {
      return this.parameter == other.parameter;
    }
    return false;
  }
}

class MyTypeExtendedMyType extends Comparable<MyTypeExtendedMyType> {
  MyTypeExtendedMyType(this.parameter);
  String parameter;

  @override
  int compareTo(other) {
    // `other` is of type `MyType` (therefore can't be `null`).
    return this.parameter.compareTo(other.parameter);
  }

  // The commented out code below is not valid:
  //
  // bool operator ==(MyTypeExtendedMyType other) {
  //   return this.parameter == other.parameter;
  // }

  // `==` needs an `Object?` as a parameter.
  bool operator ==(Object? other) {
    if (other is MyTypeExtendedMyType) {
      return this.parameter == other.parameter;
    }
    return false;
  }
}

MyType a = MyType("hello");
MyType b = MyType("hello");
MyType c = MyType("ciao");

ObjectExtendedMyType objectExtA = ObjectExtendedMyType("hello");
ObjectExtendedMyType objectExtB = ObjectExtendedMyType("hello");
ObjectExtendedMyType objectExtC = ObjectExtendedMyType("ciao");

MyTypeExtendedMyType myTypeExtA = MyTypeExtendedMyType("hello");
MyTypeExtendedMyType myTypeExtB = MyTypeExtendedMyType("hello");
MyTypeExtendedMyType myTypeExtC = MyTypeExtendedMyType("ciao");

main(List<String> args) {
  typeDefinedCompareToDemo();
  typeDefinedEqualsDemo();
  isSortedDemo();
  isReverslySortedDemo();
  findExtremaDemo();
}

void typeDefinedCompareToDemo() {
  print('typeDefinedCompareTo<num>(3, 7): ${typeDefinedCompareTo<num>(3, 7)}');
  print('typeDefinedCompareTo<num>(7, 3): ${typeDefinedCompareTo<num>(7, 3)}');
  print('typeDefinedCompareTo<num>(3, 3): ${typeDefinedCompareTo<num>(3, 3)}');

  // print('typeDefinedCompareTo<MyType>(a, b): '
  // '${typeDefinedCompareTo<MyType>(a, b)}');
  // print('typeDefinedCompareTo<MyType>(a, c): '
  // '${typeDefinedCompareTo<MyType>(a, c)}');
  // Can't be called because MyType doesn't implement `Comparable.compareTo`.

  // The following 2 print statements are not valid because
  // `typeDefinedCompareTo` requires `<T extends Comparable<T>>` and not
  // `<T extends Comparable<Object?>>`, i.e., `<T extends Comparable>`).

  // print('typeDefinedCompareTo<ObjectExtendedMyType>(a, b): '
  // '${typeDefinedCompareTo<Object?>(objectExtA, objectExtB)}');

  // print('typeDefinedCompareTo<ObjectExtendedMyType>(a, c): '
  // '${typeDefinedCompareTo<Object?>(objectExtA, objectExtC)}');

  // The two `typeDefinedEquals` calls below are implemented the same exact way
  // => same results.
  print('typeDefinedCompareTo<MyTypeExtendedMyType>(a, b): '
      '${typeDefinedCompareTo<MyTypeExtendedMyType>(myTypeExtA, myTypeExtB)}');
  print('typeDefinedCompareTo<MyTypeExtendedMyType>(a, c): '
      '${typeDefinedCompareTo<MyTypeExtendedMyType>(myTypeExtA, myTypeExtC)}');
}

void typeDefinedEqualsDemo() {
  print('typeSafeEquals<num>(3, 7): ${typeDefinedEquals<num>(3, 7)}');
  print('typeSafeEquals<num>(7, 3): ${typeDefinedEquals<num>(7, 3)}');
  print('typeSafeEquals<num>(3, 3): ${typeDefinedEquals<num>(3, 3)}');

  print('typeDefinedEquals<MyType>(a, b): ${typeDefinedEquals<MyType>(a, b)}');
  print('typeDefinedEquals<MyType>(a, c): ${typeDefinedEquals<MyType>(a, c)}');

  print('typeDefinedEquals<ObjectExtendedMyType>(a, b): '
      '${typeDefinedEquals<ObjectExtendedMyType>(objectExtA, objectExtB)}');
  print('typeDefinedEquals<ObjectExtendedMyType>(a, c): '
      '${typeDefinedEquals<ObjectExtendedMyType>(objectExtA, objectExtC)}');
  // The two `typeDefinedEquals` calls below are implemented the same exact way
  // => same results.
  print('typeDefinedEquals<MyTypeExtendedMyType>(a, b): '
      '${typeDefinedEquals<MyTypeExtendedMyType>(myTypeExtA, myTypeExtB)}');
  print('typeDefinedEquals<MyTypeExtendedMyType>(a, c): '
      '${typeDefinedEquals<MyTypeExtendedMyType>(myTypeExtA, myTypeExtC)}');
}

void isSortedDemo() {
  print('isSorted(${list1.toList()}): ${isSorted(list1)}.');
  print('isSorted(${list2.toList()}): ${isSorted(list2)}.');
  print('isSorted(${list3.toList()}): ${isSorted(list3)}.');
  print('isSorted(${list4.toList()}): ${isSorted(list4)}.');
}

void isReverslySortedDemo() {
  print('isReverslySorted(${list1.toList()}): ${isReverslySorted(list1)}.');
  print('isReverslySorted(${list2.toList()}): ${isReverslySorted(list2)}.');
  print('isReverslySorted(${list3.toList()}): ${isReverslySorted(list3)}.');
  print('isReverslySorted(${list4.toList()}): ${isReverslySorted(list4)}.');
}

void findExtremaDemo() {
  try {
    print(findExtrema(emptyListDemo));
  } on InvalidIndexException catch (exception) {
    print('Catched exception message: ' + exception.message);
  }
  print(findExtrema(oneElementListDemo));
  print(findExtrema(multipleElementListDemo));
}
