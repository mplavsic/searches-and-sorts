library searches_and_sorts;

export './src/utils.dart'
    show
        typeDefinedCompareTo,
        typeDefinedEquals,
        isSorted,
        isReverslySorted,
        findExtrema;
