import '../utils.dart';

/// Sorts a list using selection sort. You can opt to sort the list only
/// between [left] (inclusive) and [right] (exclusive).
///
/// If [compare] is omitted, this falls back to calling [Comparable.compareTo]
/// on the objects. In this case, the objects must be [Comparable].
///
/// This sorting algorithm is adequate for lists that don't contain many
/// elements, e.g., 40 or less. However, insertion sort is preferable.
///
/// This insertion sort implementation is stable.
///
/// Worst-case time complexity: O(n^2)
/// Average-case time complexity: O(n^2)
/// Best-case time complexity: O(n))
/// Worst-case space complexity: O(1) auxiliary memory
void selectionSort<E>(List<E> list,
    {int left = 0, int? right, int Function(E, E)? compare}) {
  right = RangeError.checkValidRange(left, right, list.length, 'left', 'right');

  compare ??= fallbackCompare;

  for (int pos = left; pos < right - 1; pos++) {
    E element = list[pos];
    int usMin = pos;
    // The left-most element of the unsorted sublist is our initial min.
    int usCur = pos + 1; // Current pointer

    while (usCur < right) {
      int compareResult = compare(list[usCur], element);
      if (compareResult < 0) {
        // A new min has been found...
        element = list[usCur];
        usMin = usCur;
        // ... and now marked as usMin.
      }
      usCur++;
    }
    swap(list, usMin, pos);
    // The swapping takes place only once (and not after each iteration as it
    // happens with the simple selection sort algorithm).
  }
}
