import '../utils.dart';
import '../custom_exceptions.dart';

/// Returns position of [value] inside [sortedList] using binary search.
///
/// If [left] (inclusive) and [right] (exclusive) are supplied, only that range
/// is considered.
///
/// If [compare] is omitted, this defaults to calling [Comparable.compareTo] on
/// the objects. In this case, the objects must be [Comparable].
///
/// If the list isn't sorted according to [compare], an [UnsortedListException]
/// will be thrown.
///
/// /// Returns -1 if [value] is not inside [list].
int binarySearch<E>(List<E> sortedList, E value,
    {int Function(E, E)? compare, int left = 0, int? right}) {
  right = RangeError.checkValidRange(
      left, right, sortedList.length, 'left', 'right');

  compare ??= fallbackCompare; // `??=` means assign only if the variable (in
  // this case `compare`) is null (i.e. either no compare parameter was given or
  // `null` was explicitly passed).

  if (!isSorted(sortedList, left: left, right: right, compare: compare)) {
    throw UnsortedListException();
  }

  while (left < right!) {
    int middle = left + ((right - left) >> 1); // alternative ways to write it:
    // 1. (left + right) / 2
    // 2. (left + ((right - left) / 2);
    // 3. (left + right) >>> 1 (not possible in Dart, since: the integers work
    //     differently than Java and the logical shift operation was removed)
    E element = sortedList[middle];

    int compareResult = compare(element, value); // Works with generics and
    // custom compare functions.
    if (compareResult < 0) {
      left = middle + 1;
    } else if (compareResult > 0) {
      right = middle;
    } else {
      return middle;
    }
  }

  return -1; // The searched value is not present in the list.
}
