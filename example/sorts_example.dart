import 'package:searches_and_sorts/sorts.dart';

List<int> listDemo = <int>[1000, 12, 13, 463, 802, 345, 479, -37, 2111];

void main(List<String> args) {
  print('listDemo: ${listDemo.toString()}');
  print('');

  Map<Function, String> sorts = <Function, String>{
    heapsort: 'heapsort',
    insertionSort: 'insertion sort',
    mergeSort: 'merge sort',
    selectionSort: 'selection sort',
  };

  sorts.forEach((sortImplementation, sortName) {
    print('------------------------------');
    print('');

    print('A copy of the whole list will be sorted using ${sortName}.');
    List<int> clonedList = List<int>.from(listDemo);
    sortImplementation(clonedList);
    print('Final result: ${clonedList.toString()}');

    print('');

    int indexLeft = 4;
    int indexRight = listDemo.length;
    print('A copy of the whole list will be partially sorted ('
        'from index ${indexLeft}(inclusive) until ${indexRight} (exclusive))'
        ' using ${sortName}.');
    List<int> anotherClonedList = List<int>.from(listDemo);
    sortImplementation(anotherClonedList, left: indexLeft, right: indexRight);
    print('Final result: ${anotherClonedList.toString()}');

    print('');
  });
}
