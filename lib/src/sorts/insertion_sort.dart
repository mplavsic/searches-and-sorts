import '../utils.dart';

/// Sorts a list using insertion sort. You can opt to sort the list only
/// between [left] (inclusive) and [right] (exclusive).
///
/// If [compare] is omitted, this falls back to calling [Comparable.compareTo]
/// on the objects. In this case, the objects must be [Comparable].
///
/// This sorting algorithm is adequate for lists that don't contain many
/// elements, e.g., 40 or less.
///
/// This insertion sort implementation is stable.
///
/// Worst-case time complexity: O(n^2)
/// Average-case time complexity: O(n^2)
/// Best-case time complexity: O(n^2))
/// Worst-case space complexity: O(1) auxiliary memory
void insertionSort<E>(List<E> list,
    {int left = 0, int? right, int Function(E, E)? compare}) {
  right = RangeError.checkValidRange(left, right, list.length, 'left', 'right');

  compare ??= fallbackCompare;

  for (int pos = left + 1; pos < right; pos++) {
    E element = list[pos];
    int ssMin = left; // The sorted sublist min is always the left-most element
    int ssMax = pos; // upper bound (exclusive).
    while (ssMin < ssMax) {
      int ssMiddle = ssMin + ((ssMax - ssMin) >> 1);
      int compareResult = compare(element, list[ssMiddle]);
      if (compareResult >= 0) {
        ssMin = ssMiddle + 1;
      } else {
        ssMax = ssMiddle;
      }
    }
    list.setRange(ssMin + 1, pos + 1, list, ssMin); // `pos + 1` is not included
    // Shifts elements to the right by one.
    list[ssMin] = element;
    // `ssMax` is now the max in the sorted subarray.
  }
}
