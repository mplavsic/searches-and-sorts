import 'package:searches_and_sorts/exceptions.dart';
import 'package:searches_and_sorts/searches.dart';

void main(List<String> args) {
  List<int> sortedListDemo = <int>[2, 4, 5, 6, 7, 8, 8, 9, 10];

  print('binary search');
  print(binarySearch(sortedListDemo, 6)); // 3
  print(binarySearch(sortedListDemo, 2)); // 0
  print(binarySearch(sortedListDemo, 10)); // 8
  print(binarySearch(sortedListDemo, 8)); // 6
  print(binarySearch(sortedListDemo, 6, left: 1, right: 5)); // 3
  print(binarySearch(sortedListDemo, 2, left: 3, right: 5)); // -1
  print(binarySearch(sortedListDemo, 10, left: 7, right: 9)); // 8
  print(binarySearch(sortedListDemo, 8, left: 1, right: 1)); // -1
  print('');
  print('linear search');
  print(linearSearch(sortedListDemo, 6)); // 3
  print(linearSearch(sortedListDemo, 2)); // 0
  print(linearSearch(sortedListDemo, 10)); // 8
  print(linearSearch(sortedListDemo, 8)); // 6
  print(linearSearch(sortedListDemo, 6, left: 1, right: 5)); // 3
  print(linearSearch(sortedListDemo, 2, left: 3, right: 5)); // -1
  print(linearSearch(sortedListDemo, 10, left: 7, right: 9)); // 8
  print(linearSearch(sortedListDemo, 8, left: 1, right: 1)); // -1
  print('');
  print('sentinel linear search');
  print(sentinelLinearSearch(sortedListDemo, 6)); // 3
  print(sentinelLinearSearch(sortedListDemo, 2)); // 0
  print(sentinelLinearSearch(sortedListDemo, 10)); // 8
  print(sentinelLinearSearch(sortedListDemo, 8)); // 6
  print(sentinelLinearSearch(sortedListDemo, 6, left: 1, right: 5)); // 3
  print(sentinelLinearSearch(sortedListDemo, 2, left: 3, right: 5)); // -1
  print(sentinelLinearSearch(sortedListDemo, 10, left: 7, right: 9)); // 8
  print(sentinelLinearSearch(sortedListDemo, 8, left: 1, right: 1)); // -1

  print('');
  print('');
  List<int> unsortedListDemo = <int>[8, 9, 5, 7, 2, 8, 10, 6, 4];

  print('binary search');
  try {
    print(binarySearch(unsortedListDemo, 6));
  } on UnsortedListException catch (exception) {
    print(exception);
  }
  try {
    print(binarySearch(unsortedListDemo, 2));
  } on UnsortedListException catch (exception) {
    print(exception);
  }
  try {
    print(binarySearch(unsortedListDemo, 10));
  } on UnsortedListException catch (exception) {
    print(exception.message); // message
  }
  try {
    print(binarySearch(unsortedListDemo, 8));
  } on UnsortedListException catch (exception) {
    print('binarySearch(unsortedListDemo, 8)'
            ' throws the following exception message: ' +
        exception.message);
  }
  print('');
  print('linear search');
  print(linearSearch(unsortedListDemo, 6)); // 7
  print(linearSearch(unsortedListDemo, 2)); // 4
  print(linearSearch(unsortedListDemo, 10)); // 6
  print(linearSearch(unsortedListDemo, 8)); // 0
  print('');
  print('sentinel linear search');
  print(sentinelLinearSearch(unsortedListDemo, 6)); // 7
  print(sentinelLinearSearch(unsortedListDemo, 2)); // 4
  print(sentinelLinearSearch(unsortedListDemo, 10)); // 6
  print(sentinelLinearSearch(unsortedListDemo, 8)); // 0
}
